### O que é um Commit?

No Git, um **commit** representa o estado do seu código em um ponto específico no tempo. Ele contém metadados (autor, timestamp, mensagem do commit etc.) e é utilizado para salvar progressos, registrar mudanças e integrar partes do desenvolvimento com o trabalho de outros.

---

### Características de um Bom Commit

1. **Atômico e Focado**:  
   Um commit deve ser atômico, ou seja, representar **uma única mudança lógica**. Evite misturar alterações independentes em um único commit.  
   **Exemplo:**  
   ```bash
   # Bom commit
   git commit -m "<prefix>:Add user authentication"
   
   # Mau commit
   git commit -m "Add user authentication and update UI styles"
   ```

2. **Mensagem Descritiva**:  
   Uma boa mensagem de commit explica claramente **o que foi feito** e **por que a mudança foi necessária**. Deve conter contexto suficiente para que outros (ou você no futuro) entendam a alteração sem precisar ler o código.  
   **Exemplo:**  
   ```bash
   # Boa mensagem
   git commit -m "<prefix>:Fix null pointer exception in user login"
   
   # Má mensagem
   git commit -m "Fix bug"
   ```

3. **Seguir Diretrizes Convencionais de Commits**:  
   Utilize padrões como o **Conventional Commits**, que mantêm o histórico limpo e organizado. Normalmente, seguem um formato de **tipo** (feat, fix, chore, refactor, docs) seguido de um resumo e, ocasionalmente, uma explicação detalhada ou referência a issues relacionadas.  
   **Exemplo:**  
   ```bash
   # Mensagens seguindo diretrizes
   git commit -m "feat(auth): add JWT-based authentication"
   git commit -m "fix(login): resolve race condition in login flow"
   ```

4. **Testado e Verificado**:  
   Antes de fazer o commit, garanta que as alterações foram testadas e estão funcionando corretamente. Código quebrado ou não testado pode interromper o fluxo de trabalho da equipe.

5. **Escopo Apropriado**:  
   Certifique-se de que o commit abrange apenas o escopo da tarefa em questão. Inclua todas as mudanças relacionadas a um recurso ou correção em um único commit, evitando deixar o código em um estado inconsistente.  
   **Exemplo:**  
   ```bash
   # Bom commit
   git commit -m "refactor(auth): split auth logic into separate module"
   
   # Mau commit
   git commit -m "refactor and minor fixes"
   ```

---

### Características de um Mau Commit

1. **Grande e Sem Foco**:  
   Commits com muitas alterações são difíceis de entender, revisar e depurar.  
   **Exemplo:**  
   ```bash
   # Mau commit
   git commit -m "Update project"
   ```

2. **Mensagens Vagas ou Enganosas**:  
   Mensagens de commit vagas ou enganosas não fornecem informações úteis sobre as mudanças.  
   **Exemplo:**  
   ```bash
   # Mau commit
   git commit -m "Stuff"
   ```

3. **Alterações Não Relacionadas**:  
   Combinar alterações não relacionadas em um único commit dificulta a revisão e pode introduzir bugs.  
   **Exemplo:**  
   ```bash
   # Mau commit
   git commit -m "Update readme and fix login issue"
   ```

4. **Código Incompleto ou Não Testado**:  
   Cometer código incompleto ou não testado pode causar problemas para outros membros da equipe e quebrar a build.

5. **Falta de Contexto**:  
   Commits sem contexto tornam difícil entender por que a mudança foi feita, causando confusão ao revisitar o código no futuro.

---

### Boas Práticas para Commits

1. **Comite com Frequência, mas Não em Excesso**:  
   Mantenha um equilíbrio entre commits frequentes e espaçados. Cada commit deve representar uma mudança significativa e coesa.

2. **Escreva Mensagens Claras e Descritivas**:  
   Explique o que o commit faz e o motivo da mudança.

3. **Use Branches de Forma Eficiente**:  
   Crie branches para novos recursos, correções de bugs e experimentos. Utilize Pull Requests para revisão e integração ao branch principal.

4. **Revise e Agrupe Commits**:  
   Antes de mesclar uma branch, revise os commits e agrupe alterações pequenas ou correções em unidades lógicas, utilizando o comando `git rebase` para manter o histórico limpo.

5. **Automatize Testes**:  
   Utilize ferramentas de integração contínua para testar automaticamente o código em cada commit, reduzindo o risco de introduzir bugs.

