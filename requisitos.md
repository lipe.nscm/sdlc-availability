## Requisitos Funcionais

**Requisitos funcionais** descrevem as funcionalidades específicas que o sistema deve executar. Eles detalham o comportamento do sistema em resposta a entradas externas e especificam as ações que o sistema deve realizar.

### Critérios para Definição de Requisitos Funcionais:

1. **Clareza e Precisão:**
   - Devem ser escritos de forma clara, sem ambiguidades.
   - Cada requisito deve ter uma única interpretação.

2. **Completude:**
   - Todos os aspectos funcionais necessários para o sistema estão cobertos.
   - Nenhuma funcionalidade importante está omitida.

3. **Consistência:**
   - Não deve haver contradições entre os requisitos.
   - Deve harmonizar-se com outros requisitos funcionais e não funcionais.

4. **Verificabilidade:**
   - Deve ser possível testar ou verificar se o requisito foi atendido.
   - Requisitos devem ser mensuráveis ou observáveis.

5. **Viabilidade:**
   - Deve ser possível implementar o requisito dentro das restrições de tempo, custo e tecnologia.

6. **Necessidade:**
   - O requisito deve agregar valor ao sistema e ser necessário para atender às necessidades dos usuários ou stakeholders.

7. **Prioridade:**
   - Classificação da importância do requisito, ajudando na gestão de recursos e no planejamento do desenvolvimento.

8. **Rastreabilidade:**
   - Cada requisito deve estar ligado a uma necessidade ou objetivo específico.
   - Facilita a gestão de mudanças e a manutenção futura.

### Exemplos de Requisitos Funcionais:

- O sistema deve permitir que usuários registrem-se com um endereço de e-mail e senha.
- O sistema deve gerar relatórios financeiros mensais.
- O sistema deve enviar notificações por e-mail quando uma nova tarefa for atribuída.

## Requisitos Não Funcionais

**Requisitos não funcionais** especificam os critérios que julgam a operação de um sistema, ao invés de comportamentos específicos. Eles estão relacionados à qualidade, desempenho e restrições do sistema.

### Critérios para Definição de Requisitos Não Funcionais:

1. **Clareza e Precisão:**
   - Devem ser descritos de forma objetiva e sem ambiguidades.
   - Especificações devem ser detalhadas para evitar interpretações variadas.

2. **Mensurabilidade:**
   - Devem ser quantificáveis para permitir avaliação objetiva.
   - Exemplos incluem tempos de resposta, níveis de segurança, etc.

3. **Relevância:**
   - Devem ser pertinentes às necessidades dos usuários e aos objetivos do sistema.
   - Devem contribuir para a qualidade geral do sistema.

4. **Consistência:**
   - Devem ser consistentes entre si e com os requisitos funcionais.
   - Evitar conflitos entre diferentes requisitos não funcionais.

5. **Viabilidade:**
   - Deve ser possível atender ao requisito dentro das limitações tecnológicas e de recursos.
   - Considerar restrições de orçamento, tempo e tecnologia disponível.

6. **Prioridade:**
   - Determinar a importância relativa dos requisitos não funcionais.
   - Ajudar na alocação de recursos para atender às necessidades mais críticas primeiro.

7. **Flexibilidade e Escalabilidade:**
   - Considerar a capacidade do sistema de se adaptar a mudanças futuras.
   - Planejar para crescimento e expansão sem comprometer a performance.

8. **Segurança e Confiabilidade:**
   - Definir padrões de segurança para proteção de dados e operações.
   - Estabelecer níveis de confiabilidade e disponibilidade do sistema.

### Exemplos de Requisitos Não Funcionais:

- O sistema deve suportar até 10.000 usuários simultaneamente sem degradação de desempenho.
- O tempo de resposta para qualquer solicitação do usuário deve ser inferior a 2 segundos.
- O sistema deve estar disponível 99,9% do tempo durante o ano.
- Os dados devem ser criptografados utilizando o padrão AES-256.
- A interface do usuário deve ser compatível com os principais navegadores (Chrome, Firefox, Safari, Edge).

## Boas Práticas na Definição de Requisitos

1. **Engajamento dos Stakeholders:**
   - Incluir todas as partes interessadas no processo de definição para garantir que todas as necessidades sejam consideradas.

2. **Uso de Linguagem Clara:**
   - Evitar jargões técnicos quando não forem necessários e garantir que todos os envolvidos compreendam os requisitos.

3. **Documentação Adequada:**
   - Manter registros detalhados e organizados dos requisitos para facilitar futuras referências e alterações.

4. **Revisões e Validações Regulares:**
   - Realizar sessões de revisão com stakeholders para validar e ajustar os requisitos conforme necessário.

5. **Gerenciamento de Mudanças:**
   - Estabelecer um processo para gerenciar alterações nos requisitos de forma controlada, minimizando impactos negativos no projeto.

